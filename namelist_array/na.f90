program na

  implicit none

  integer, parameter :: dp = KIND(0.0d0)
  integer :: iounit, ierr
  real(dp) :: array(5)
  character(len=32) :: strings(5)

  namelist /test/ array, strings

  array = -1.0d0
  strings = 'foo'

  open(newunit=iounit, file='test.nml', status='old', iostat=ierr)
  read(iounit, nml=test, iostat=ierr)

  write(*,*) array
  write(*,*) strings

end program na
