program generics

  implicit none

  integer, parameter :: dp = kind(0.0d0)
  real(dp) :: x
  integer :: i
  character(len=32) :: name

  interface check_positive
     procedure check_positive_int
     procedure check_positive_dp
  end interface check_positive

  name = trim('blah')
  i = 1
  x = 0.0d0

  call check_positive(name, i)
  call check_positive(name, x)

contains

     subroutine check_positive_int(name, val, lower, upper)
       character(*) :: name
       integer :: val
       integer, optional :: lower, upper
  
       write(*,*) name, val
     end subroutine check_positive_int

     subroutine check_positive_dp(name, val, lower, upper)
       character(*) :: name
       real(dp) :: val
       real(dp), optional :: lower, upper
  
       write(*,*) name, val
     end subroutine check_positive_dp

end program generics
