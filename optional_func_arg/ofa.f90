program ofa

  implicit none

  integer, parameter :: dp = KIND(0.0d0)

  real(dp) :: z
  integer :: i

  i = 0
  
  z = maybe_set_ierr(1.2d0)
  write(*,*) z, i

  z = maybe_set_ierr(1.2d0, i)
  write(*,*) z, i

contains

  real(dp) function maybe_set_ierr(x, j) result(y)
    real(dp), intent(in) :: x
    integer, optional, intent(out) :: j

    y = x*x
    if (present(j)) j = 1
  end function maybe_set_ierr

end program ofa
