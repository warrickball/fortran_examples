! https://stackoverflow.com/questions/14420740/how-to-use-reduction-on-an-array-in-fortran

! do 100 k=1,lines

! co_tmp = 0.0
! si_tmp = 0.0
! !$OMP PARALLEL DO PRIVATE(dotprod) REDUCTION(+:co_tmp,si_tmp)
!  do 110,i=1,ION_COUNT
!   dotprod=(rx(k)*x(i)+ry(k)*y(i)...)
!   co_tmp=co_tmp+COS(dotprod)
!   si_tmp=si_tmp+SIN(dotprod)
!  110 continue
! !$OMP END PARALLEL DO

! co(k) = co_tmp
! si(k) = si_tmp

! 100 continue

! Learning to properly accumulate sums in parallel with OpenMP.

program reduce

  integer, parameter :: dp = kind(0.0d0)
  integer :: k, n
  real(dp), allocatable :: x(:), dx(:), t(:)

  n = 3
  allocate(x(n), dx(n), t(n))
  x = 0d0

  t = [(k, k = 1, n)]
  
  do k = 1, 20
     call do_something(t, dx)
     x = x + dx
  end do

  write(*,*) '        serial: ', x

  x = 0d0
  !$OMP PARALLEL DO
  do k = 1, 20
     call do_something(t, dx)
     x = x + dx
  end do
  !$OMP END PARALLEL DO
  
  write(*,*) 'naive parallel: ', x

  x = 0d0
  !$OMP PARALLEL DO PRIVATE(dx) REDUCTION(+:x)
  do k = 1, 20
     call do_something(t, dx)
     x = x + dx
  end do
  !$OMP END PARALLEL DO
  
  write(*,*) 'smart parallel: ', x

  deallocate(x, dx, t)

contains

  subroutine do_something(t, dx)
    real(dp), intent(in) :: t(:)
    real(dp), intent(out) :: dx(:)
    
    dx = sin(t)**2
  end subroutine do_something

end program reduce
