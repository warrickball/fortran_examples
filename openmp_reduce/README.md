# reduce

A simple example of how to safely create an OpenMP accumulator.  Based
on the example from
[StackOverflow](https://stackoverflow.com/questions/14420740/how-to-use-reduction-on-an-array-in-fortran).
Compile with e.g. `gfortran reduce.f90 -fopenmp -o reduce.x`.
