program ieee
  
  use IEEE_ARITHMETIC

  integer, parameter :: dp = kind(0.0d0)
  
  real(dp) :: x

  x = 0.1234869428d0
  write(*,*) IEEE_IS_FINITE(x), x

  x = 0d0
  x = x/0d0
  write(*,*) IEEE_IS_FINITE(x), x

  x = 1d0
  x = x/0d0
  write(*,*) IEEE_IS_FINITE(x), x

end program ieee
