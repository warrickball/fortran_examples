program fractals

   implicit none
  
   integer, parameter :: dp = KIND(0.0d0)
   real(dp), parameter :: PI = 3.141592653589793d0

   real(dp) :: x, xmin, xmax, y, ymin, ymax
   integer  :: xres, yres, maxiter

   complex(dp), allocatable :: z(:,:)
   integer :: i, j
   integer :: iounit, ierr

   ierr = 0

   ! set default parameters
   maxiter = 1000

   xmin = 0.35*PI
   xmax = 0.65*PI
   xres = 301

   ymin = -0.25
   ymax = 0.25
   yres = 301

   call parse_args

   allocate(z(xres, yres))
   
   do i = 1, xres
      x = xmin + (xmax-xmin)*real(i-1, dp)/(xres-1)
      do j = 1, yres
         y = ymin + (ymax-ymin)*real(j-1, dp)/(yres-1)
         z(i,j) = cmplx(x, y, dp)
      end do
   end do

   !$OMP PARALLEL DO PRIVATE(i,j) SHARED(z)
   do j = 1, yres
      do i = 1, maxiter
         z(:,j) = z(:,j) - tan(z(:,j))
      end do
   end do
   !$OMP END PARALLEL DO

   open(newunit=iounit, file='fractals.bin', access='stream', status='replace')
   if (ierr /= 0) stop 'error opening fractals.bin'
   write(iounit) 0.0d0
   write(iounit) xmin, xmax, xres
   write(iounit) ymin, ymax, yres
   write(iounit) z
   close(iounit)

   deallocate(z)

contains

   subroutine parse_args
      character(80) :: arg
      integer :: i

      arg = '78yjaisd'
      i = 0
      do while (arg /= ' ')
         i = i+1
         call get_command_argument(i, arg)
         i = i+1
         select case (arg)
         case('--xres')
            call get_command_argument(i, arg)
            read(arg, *) xres
         case('--yres')
            call get_command_argument(i, arg)
            read(arg, *) yres
         case('--maxiter')
            call get_command_argument(i, arg)
            read(arg, *) maxiter
         end select
      end do

   end subroutine parse_args
  
end program fractals
