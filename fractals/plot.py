#!/usr/bin/env python3

from argparse import ArgumentParser
import numpy as np
import matplotlib.pyplot as pl

parser = ArgumentParser()
parser.add_argument('-i', '--input', type=str,
                    default='fractals.bin')
parser.add_argument('-o', '--output', type=str,
                    default='fractals.png')
parser.add_argument('--tol', type=float, default=-1)
args = parser.parse_args()

with open(args.input, 'rb') as f:
    tol, = np.fromfile(f, dtype=np.float64, count=1)*10
    xmin, xmax = np.fromfile(f, dtype=np.float64, count=2)
    xres, = np.fromfile(f, dtype=np.int32, count=1)
    ymin, ymax = np.fromfile(f, dtype=np.float64, count=2)
    yres, = np.fromfile(f, dtype=np.int32, count=1) 
    z = np.fromfile(f, dtype=np.complex128).reshape((xres, yres))

if args.tol >= 0:
    tol = args.tol

if tol > 0:
    z[np.abs(z.imag) >= tol] = 1e99*np.pi
    z = np.around(z/np.pi)

a = np.unique(z, return_inverse=True)[1]

c = np.random.rand(a.max()+1)
c = (c-c.min())/(c.max()-c.min())
pl.imshow(c[a.reshape(*z.shape)], cmap='CMRmap', aspect='auto',
          origin='lower', extent=(xmin,xmax,ymin,ymax))
pl.xlabel('Re(z)')
pl.ylabel('Im(z)')
pl.savefig(args.output)
