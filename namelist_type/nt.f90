program nt

  implicit none

  integer, parameter :: dp = KIND(0.0d0)

  type my_type
     real(dp) :: x
     integer :: i
  end type my_type

  integer :: iounit
  type(my_type) :: t(2)

  namelist /nml/ t

  open(newunit=iounit, file='nt.nml', status='old')
  read(iounit, nml)
  close(iounit)

  write(*,*) 't%i', t%i
  write(*,*) 't%x', t%x

  write(*,*) 't(1)', t(1)
  write(*,*) 't(2)', t(2)

  write(*,*) 't', t

end program nt
