program mm
  integer, parameter :: dp=kind(0.0d0)
  real(dp) :: A(2,3), B(3,2), C(2,2)
  integer :: i, j

  ! A = [[1,2,3],[4,5,6]]
  ! B = [[1,2],[3,4],[5,6]]
  A(1,:) = [1,2,3]
  A(2,:) = [4,5,6]
  B(1,:) = [1,2]
  B(2,:) = [3,4]
  B(3,:) = [5,6]

  C = matmul(A, B)

  do i=1,2
     write(*,*) C(i,:)
  end do

  do i=1,2
     do j=1,2
        C(i,j) = dot_product(A(i,:), B(:,j))
     end do
  end do

  do i=1,2
     write(*,*) C(i,:)
  end do
  
end program mm
