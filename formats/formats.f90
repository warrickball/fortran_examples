program formats

  implicit none

  integer, parameter :: dp = kind(0.0d0)

  real(dp) :: x

  x = -1.122858726598856238475692873493518d7

  write(*, '(1pes15.6)') x
  write(*, '(1pe15.6)') x
  write(*, '(1e15.6)') x
  write(*, '(3x,1pe15.6)') x
  write(*, '(1pe15.6)') x
  write(*, '(tr1,d16.10)') x
  write(*, '(1x,d16.10)') x
  write(*, '(d17.10)') x

end program formats
