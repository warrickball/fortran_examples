program compare_random

  implicit none

  integer, parameter :: dp = kind(0.0d0)
  integer, parameter :: N = 100
  
  ! parameters from Python
  integer, parameter :: n_zig = 256
  real(dp), parameter :: r_zig = 3.654152885361008796d0
  real(dp), parameter :: V_zig = 4.928673233974657959d-3
  ! integer, parameter :: n_zig = 4096
  ! real(dp), parameter :: r_zig = 4.385945034871304493d0
  ! real(dp), parameter :: V_zig = 3.061541032784648240d-4

  real(dp) :: x_zig(0:n_zig)
  
  real(dp) :: t0, t1, x(1000000)
  ! real(dp) :: mean, var
  integer :: i, iounit, ierr

  call cpu_time(t0)
  
  do i = 1, N
     call marsaglia_polar(x)
  end do

  call cpu_time(t1)

  open(newunit=iounit, file='marsaglia_polar.dat', status='replace', iostat=ierr)
  write(iounit, '(f23.17)') x
  close(iounit)

  write(*,*) 'Marsaglia polar: ', t1-t0
  call check(x)

  call cpu_time(t0)

  do i = 1, N
     call leva(x)
  end do

  call cpu_time(t1)

  open(newunit=iounit, file='leva.dat', status='replace', iostat=ierr)
  write(iounit, '(f23.17)') x
  close(iounit)

  write(*,*) 'Leva:            ', t1-t0
  call check(x)

  call cpu_time(t0)
  call init_zig
  call cpu_time(t1)
  write(*,*) 'Ziggurat init:   ', t1-t0

  call cpu_time(t0)

  do i = 1, N
     call zig(x)
  end do

  call cpu_time(t1)

  open(newunit=iounit, file='ziggurat.dat', status='replace', iostat=ierr)
  write(iounit, '(f23.17)') x
  close(iounit)
  
  write(*,*) 'Ziggurat:        ', t1-t0
  call check(x)

contains

  elemental real(dp) function g(x)
    real(dp), intent(in) :: x

    g = exp(-x**2/2)
  end function g

  elemental real(dp) function ginv(x)
    real(dp), intent(in) :: x

    ginv = sqrt(-2*log(x))
  end function ginv

  subroutine init_zig
    integer :: i

    x_zig = 0
    
    x_zig(1) = r_zig
    do i = 2, n_zig
       x_zig(i) = ginv(g(x_zig(i-1)) + V_zig/x_zig(i-1))
    end do
    x_zig(n_zig) = 0

    x_zig(0) = V_zig/g(r_zig)

    ! write(*,*)
    ! write(*,*) 'absolute error in volume of last block of ziggurat:'
    ! write(*,*) g(x_zig(n_zig-1)) + V_zig/x_zig(n_zig-1) - 1
    ! write(*,*)
  end subroutine init_zig

  subroutine zig(x)
    real(dp), intent(out) :: x(:)
    integer :: i, k, n
    real(dp) :: y, z

    x = 0
    n = size(x)

    do i = 1, n
       do ! while (.true.)
          call random_number(z)
          k = int(n_zig*z)
          call random_number(z)
          z = (2*z - 1)*x_zig(k)
          if (abs(z) < x_zig(k+1)) then
             exit
          end if

          if (k == 0) then
             z = zig_tail(z)
             exit
          else
             call random_number(y)
             if (y*(g(x_zig(k)) - g(x_zig(k+1))) < g(z) - g(x_zig(k+1))) exit
          end if
       end do

       x(i) = z
    end do

  end subroutine zig

  real(dp) function zig_tail(s)
    real(dp), intent(in) :: s
    real(dp) :: x, y

    zig_tail = 0

    do ! while (.true.)
       call random_number(x)
       call random_number(y)
       x = -log(x)/x_zig(1)
       y = -2*log(y)
       if (y > x**2) then
          zig_tail = sign(x_zig(1) + x, s)
          exit
       end if
    end do

  end function zig_tail

  subroutine marsaglia_polar(x)
    ! Populates the array ``x`` with normally-distributed random
    ! variates with zero mean and unit variance using the polar form
    ! of the Box-Müller transform.  The length of ``x`` must be even
    ! otherwise the subroutine will ``stop``.
    real(dp), intent(out) :: x(:)
    real(dp) :: w
    integer :: i, n

    n = size(x)

    ! let's just be VERY defensive for now
    if (mod(n, 2) == 1) then
       write(*,*) 'ERROR in math.randn: output array must have '
       write(*,*) '                     even number of elements'
       stop
    end if
    
    call random_number(x)  ! one initial call saves some time
    x = 2d0*x - 1d0

    do i = 1, n, 2
       w = x(i)*x(i) + x(i+1)*x(i+1)  ! was 2d0 to trigger do while
       do while (w >= 1.0)
          call random_number(x(i:i+1))
          ! x(i:i+1) = 2d0*x(i:i+1) - 1d0
          x(i) = 2d0*x(i) - 1d0
          x(i+1) = 2d0*x(i+1) - 1d0
          w = x(i)*x(i) + x(i+1)*x(i+1)
       end do

       w = sqrt(-2d0*log(w)/w)
       ! x(i:i+1) = x(i:i+1)*w
       x(i) = x(i)*w
       x(i+1) = x(i+1)*w
    end do

  end subroutine marsaglia_polar

  
  subroutine leva(x)
    real(dp) :: x(:)
    integer :: i, n

    n = size(x)
    do i = 1, n
       x(i) = do_one_leva()
    end do
  end subroutine leva

  
  real(dp) function do_one_leva()
    real(dp) :: u, v, x, y, Q
    real(dp), parameter :: s = 0.449871d0
    real(dp), parameter :: t = -0.386595d0
    real(dp), parameter :: a = 0.19600d0
    real(dp), parameter :: b = 0.25472d0

    do
       call random_number(u)
       call random_number(v)
       v = 1.7156d0*(v - 0.5d0)
       x = u - s
       y = abs(v) - t
       Q = x*x + y*(a*y - b*x)
       if (Q < 0.27597d0) then
          exit
       elseif (Q > 0.27846d0) then
          cycle
       elseif (v*v > -4d0*u*u*log(u)) then
          cycle
       end if
    end do

    do_one_leva = v/u

  end function do_one_leva

  subroutine check(x)
    real(dp), intent(in) :: x(:)
    real(dp) :: mean

    mean = sum(x)/size(x)
    write(*,*) 'mean   =', mean
    write(*,*) 'var    =', sum((x-mean)**2)/size(x)
    
  end subroutine check
  
end program compare_random
