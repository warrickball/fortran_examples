#!/usr/bin/env python3

"""Experimenting with Ziggurat Method for generating
normally-distributed random numbers.  Currently just computes
partition points.  Mostly from following

    http://www.doornik.com/research/ziggurat.pdf

and cross-checking against

    https://blogs.mathworks.com/cleve/2015/05/18/the-ziggurat-random-normal-generator/

"""

import numpy as np
import matplotlib.pyplot as pl
from scipy.integrate import quad
from scipy.optimize import fsolve
from scipy.special import erfc

def f(x):
    return np.exp(-x**2/2)

def finv(x):
    return np.sqrt(-2.*np.log(x))

def V(r):
    # return r*f(r) + quad(f, r, np.inf)[0]
    return r*f(r) + np.sqrt(np.pi/2.)*erfc(r/np.sqrt(2.))

def eq(r, C):
    xC_1 = r
    for i in range(2, C):
        xC_1 = finv(f(xC_1) + V(r)/xC_1)

    return xC_1*(f(0) - f(xC_1)) - V(r)

n = 4096  # 6 for comparison with MathWorks blog, 256 for production quality

r = 6.0

while eq(r, n) > 0.0:
    r -= 0.002

r = fsolve(eq, r, args=(n,), xtol=1e-14)[0]

print('  integer, parameter :: n_zig = %i' % n)
print('  real(dp), parameter :: r_zig = %26.18e' % r)
print('  real(dp), parameter :: V_zig = %26.18e' % V(r))

x = [r]
for i in range(2, n):
    x.append(finv(f(x[-1]) + V(r)/x[-1]))

x.append(0.0)
x.append(V(r)/f(x[0]))  # makes x[-1] sample tail correctly
x = np.squeeze(x)

def randn(n):
    while True:
        k = np.random.randint(n)
        z = np.random.uniform(-1, 1)*x[k-1]

        if abs(z) < x[k]:
            # print('inside box, k=%i' % k)
            return z
        else:
            # print('outside box, k=%i' % k)
            if k == 0:
                s = np.sign(z)
                while True:
                    z = -np.log(np.random.rand())/x[0]
                    if -2*np.log(np.random.rand()) > z**2:
                        return s*(x[0] + z)
            else:
                # print('outside box, k=%i' % k)
                if np.random.rand()*(f(x[k-1])-f(x[k])) < f(z) - f(x[k]):
                    return z

print('Sampling... ', end='', flush=True)
X = [randn(n) for i in range(100000)]
print('Done.', flush=True)
pl.hist(X, bins='auto', density=True)
xx = np.mgrid[-5:5:1001j]
pl.plot(xx, f(xx)/np.sqrt(2.*np.pi))
pl.show()
