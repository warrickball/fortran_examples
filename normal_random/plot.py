#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as pl

bins = np.mgrid[-10:10:201j]

x = np.loadtxt('marsaglia_polar.dat')
pl.hist(x, bins=bins, density=True, label='Marsaglia polar', histtype='step')

x = np.loadtxt('leva.dat')
pl.hist(x, bins=bins, density=True, label='Leva', histtype='step')

x = np.loadtxt('ziggurat.dat')
pl.hist(x, bins=bins, density=True, label='Ziggurat', histtype='step')

x = np.mgrid[-10:10:1001j]
pl.plot(x, np.exp(-x**2/2)/np.sqrt(2.*np.pi))

pl.legend()
pl.show()
