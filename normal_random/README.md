# normal_random

Compares different methods for generating normally distributed random
numbers.

`compare_random.f90` is the main source file.  Compile it with
something like

```
gfortran compare_random.f90 -o compare_random.x
```

`ziggurat.py` generates parameters for the Ziggurat method and
implements it in Python.

`plot.py` plots the output distributions as a simple check that they
look Gaussian.

The `leva` implementation appears to be buggy.
