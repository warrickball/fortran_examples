# bit_for_bit

Basic tests of bit-for-bit issues. i.e. operations that we might
expect to give the same results but don't.
