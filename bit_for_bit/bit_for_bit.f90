program bit_for_bit
  integer, parameter :: dp = kind(0.0d0)

  real(dp) :: T

  T = 0.12740751439053240897567943007426d0

  write(*, '(es26.18)') T**4
  write(*, '(es26.18)') T**4.0d0
  write(*, '(es26.18)') T*T*T*T

end program bit_for_bit
