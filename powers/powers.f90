program for_reals

  implicit none

  integer, parameter :: dp = kind(0.0d0)
  integer :: i
  real(dp) :: x

  write(*, '(7A26)') 'x', &
       'x**4', 'pow4(x)', '(x*x)*(x*x)', &
       'x**7', 'pow7(x)', '(x*x)*(x*x)*(x*x)*x'
     
  do i = 1, 100
     x = real(i, dp)/10.0_dp
     write(*, '(7es26.16)') x, &
          x**4, x*x*x*x, (x*x)*(x*x), &
          x**7, x*x*x*x*x*x*x, (x*x)*(x*x)*(x*x)*x
  end do

end program for_reals
