program linear_algebra
  ! learning to call BLAS/LAPACK
  
  implicit none

  integer, parameter :: dp = kind(0.0d0)
  integer :: ierr

  integer :: N
  real(dp), allocatable :: A(:,:), Ainv(:,:), L(:,:)
  real(dp), allocatable :: x(:,:), y(:,:)
  integer :: i, j, LDA, LDB

  ierr = 0

  ! use DPOTRF, DPOTRS to calculate a norm using a covariance matrix
  ! useful for chi^2

  N = 2
  LDA = 2
  allocate(A(N,N))
  allocate(L(LDA,N))
  allocate(x(N,1))    ! my vectors are column vectors

  A(1,:) = [ 1.0, 0.5 ]
  A(2,:) = [ 0.5, 1.0 ]
  x(:,1) = [ 1.0, 1.0 ]

  call RANDOM_NUMBER(A)
  A = MATMUL(A, TRANSPOSE(A)) ! guarantees positive-definite symmetric
  call RANDOM_NUMBER(x)

  write(*,*) 'A = '
  call print_matrix(A)
  write(*,*) 'x = '
  call print_matrix(TRANSPOSE(x))
  
  L(1:N,1:N) = A

  call DPOTRF('L', N, L, LDA, ierr)

  ! only the leading triangle is assigned, the rest of the array
  ! retains the original assignment, so we have to overwrite it?
  do i = 1, N-1
     do j = i+1, N
        L(i,j) = 0
     end do
  end do

  write(*,*) 'L = '
  call print_matrix(L)
  write(*,*) 'L*L^T = '
  call print_matrix(MATMUL(L, TRANSPOSE(L)))

  ! let's think about the norm (here, x is a column vector)
  ! we have x'.Ainv.x = (xLinv)'.(xLinv) = y'.y where Ly=x
  ! so to get y we need to solve the linear system Ly=x

  ! alternatively, we can use x'.Ainv.x = x'.y where Ay=x
  ! so now we solve the linear system Ay=x
  allocate(y(N,1))
  y = x
  LDB = N
  call DPOTRS('L', N, 1, L, LDA, y, LDB, ierr)

  ! write(*, '(99f10.5)') y
  write(*,*) 'x^T*A^{-1}*x'
  write(*,*) 'LAPACK    ', SUM(x*y)

  ! cross check by doing it manually
  allocate(Ainv(N,N))

  Ainv(1,1) = A(2,2)
  Ainv(1,2) = -A(2,1)
  Ainv(2,1) = -A(1,2)
  Ainv(2,2) = A(1,1)
  Ainv = Ainv/(A(1,1)*A(2,2)-A(1,2)*A(2,1))

  write(*,*) 'manual    ', MATMUL(MATMUL(TRANSPOSE(x), Ainv), x)
  
  ! let's check that we inverted correctly
  write(*,*) 'A*A^{-1}'
  call print_matrix(MATMUL(A, Ainv))
  write(*,*) 'A^{-1}*A'
  call print_matrix(MATMUL(Ainv, A))

  deallocate(x)
  deallocate(y)
  deallocate(A)
  deallocate(Ainv)
  deallocate(L)

contains

  subroutine print_matrix(M)

    real(dp), intent(in) :: M(:,:)
    integer :: i, j

    do i = 1, SIZE(M, 1)
       write(*, '(99f12.7)') M(i,:)
    end do
    
  end subroutine print_matrix

end program linear_algebra
